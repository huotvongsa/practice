import { Component } from "react";
import {Card, Button} from 'react-bootstrap'

export default class Item extends Component{
    constructor(){
        super()
            this.state = {
        data: [
                {
                    img: 'https://media.macphun.com/img/uploads/customer/how-to/579/15531840725c93b5489d84e9.43781620.jpg?q=85&w=1340',
                    title: 'Beautiful gril',
                    description: 'In cambodia'
                  },
                  {
                    img: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
                    title: 'France',
                    description: 'sunset'
                  },
                  {
                    img: 'https://www.planetware.com/photos-large/F/france-things-to-do-eiffel-tower-sunset.jpg',
                    title: 'toewel',
                    description: 'France'
                  } 
            ]
        }
    }  
    onDelete =(e) =>{
        let temp =[...this.state.data]
        temp.splice(e.target.name,1)
        this.setState({
            data:temp
        })
    }     
        render(){
            let myCard = this.state.data.map((item,index)=> {
            return (
                <div key={index}>
                    <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={item.img} />
                    <Card.Body>
                        <Card.Title>{item.title}</Card.Title>
                        <Card.Text>
                        {item.description}
                        </Card.Text>
                        <Button name={index} onClick={this.onDelete} variant="danger">delete</Button>
                    </Card.Body>
                    </Card>
                </div>
            )
        });        
         return <div>
             {myCard}
         </div>   
        }
    }
