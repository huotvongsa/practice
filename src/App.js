import { Col, Container, Row } from 'react-bootstrap';
import './App.css';
import ButtonGroup from './components/ButtonGroup';
import Header from './components/Header';
import Item from './components/Item';
import NavMenu from './components/NavMenu';

import React, { Component } from 'react'

export default class App extends Component {
  render() {
    return (
      <>
        <NavMenu />
        <Container>
          <Row>
            <Item></Item>
          </Row>     
        </Container>
      </>
    )
  }
}
